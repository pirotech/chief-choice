export type Rate = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10

export type Review = {
  id: string
  plate: {
    name: string
    rate: Rate
    comment: string
  }
  place: {
    name: string
  }
}
