import React from 'react'
import styles from './textarea.module.scss'

export type TextareaProps = React.TextareaHTMLAttributes<HTMLTextAreaElement> & {
  error?: string
}

export const Textarea: React.FC<TextareaProps> = React.memo((props) => {
  const { error, ...rest } = props
  return (
    <div className={styles.wrapper}>
      <textarea {...rest} />
      {error && <p className={styles.error}>{error}</p>}
    </div>
  )
})

Textarea.displayName = 'Textarea'
