import React from 'react'
import { Textarea, TextareaProps } from './textarea'
import { Control, Controller, ControllerProps } from 'react-hook-form'

type Props = TextareaProps & {
  controllerProps: Omit<ControllerProps, 'render' | 'control'> & {
    control: Control<any>
  }
}

export const TextareaController: React.FC<Props> = React.memo((props) => {
  const { controllerProps, ...rest } = props

  return (
    <Controller
      {...controllerProps}
      render={({ field }) => <Textarea {...rest} {...field} />}
    />
  )
})
