import React, { MouseEventHandler, PropsWithChildren } from 'react'
import { createPortal } from 'react-dom'
import styles from './modal.module.scss'

type Props = PropsWithChildren & {
  visible: boolean
  title: string
  onClose(): void
}

export const Modal: React.FC<Props> = React.memo((props) => {
  const { children, visible, title, onClose } = props

  const handleClose: MouseEventHandler<HTMLDivElement> = (event) => {
    if (event.target === event.currentTarget) {
      onClose()
    }
  }

  return (
    visible &&
    createPortal(
      <div className={styles.modal__wrapper} onClick={handleClose}>
        <div className={styles.modal__content}>
          <h2 className={styles.modal__title}>{title}</h2>
          {children}
        </div>
      </div>,
      document.body,
    )
  )
})
