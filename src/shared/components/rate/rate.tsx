import React from 'react'
import classNames from 'classnames'
import { Styleable } from '../../styles/utils'
import styles from './rate.module.scss'

type Props = Styleable & {
  className?: string
  value: number
}

export const Rate: React.FC<Props> = React.memo((props) => {
  return (
    <div className={classNames(styles.lines, props.className)}>
      {new Array(props.value).fill('').map((_, index) => (
        <div key={index} className={styles.line} />
      ))}
    </div>
  )
})

Rate.displayName = 'Rate'
