import React from 'react'
import styles from './input.module.scss'

export type InputProps = React.InputHTMLAttributes<HTMLInputElement> & {
  icon?: React.ReactNode;
  error?: string;
}

export const Input: React.FC<InputProps> = React.memo((props) => {
  const { icon, error, ...rest } = props

  return (
    <div className={styles.wrapper}>
      <div className={styles.inputWrapper}>
        {icon && <div className={styles.icon}>{icon}</div>}
        <input {...rest} />
      </div>
      {error && <p className={styles.error}>{error}</p>}
    </div>
  )
})

Input.displayName = 'Input'
