import React from 'react'
import { Control, Controller, ControllerProps } from 'react-hook-form'
import { Input, InputProps } from './input'

type Props = InputProps & {
  controllerProps: Omit<ControllerProps, 'render' | 'control'> & {
    control: Control<any>
  }
}

export const InputController: React.FC<Props> = React.memo((props) => {
  const { controllerProps, ...rest } = props

  return (
    <Controller
      {...controllerProps}
      render={({ field }) => <Input {...rest} {...field} />}
    />
  )
})

InputController.displayName = 'InputController'
