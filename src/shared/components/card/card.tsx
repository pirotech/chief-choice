import React from 'react'
import classNames from 'classnames'
import { Rate } from '../rate'
import { Review } from '../../../entities/review/model/types'
import styles from './card.module.scss'
import { Trash2 } from 'lucide-react'
import { Button } from '../button'
import { useShifted } from '../../hooks/useShifted'

type Props = {
  review: Review
  onRemove(id: string): void;
}

export const Card: React.FC<Props> = React.memo((props) => {
  const { review, onRemove } = props

  const { shifted, onTouchStart, onTouchMove } = useShifted()

  const handleRemove = () => onRemove(review.id)

  return (
    <li
      key={review.plate.name}
      className={classNames(styles.card, shifted && styles.card_shifted)}
      onTouchStart={onTouchStart}
      onTouchMove={onTouchMove}
    >
      <div className={styles.card__movedWrapper}>
        <div className={styles.card__content}>
          <div className={styles.card__header}>
            <h3 className={styles.card__title}>{review.plate.name}</h3>
            <span className={styles.card__place}>в {review.place.name}</span>
            <Rate value={review.plate.rate} />
          </div>
          <div className={styles.card__comment}>{review.plate.comment}</div>
        </div>
        <div className={styles.card__actions}>
          <Button onClick={handleRemove}>
            <Trash2 size={20} />
          </Button>
          {/* <Button>
            <Edit2 size={20} />
          </Button> */}
        </div>
      </div>
    </li>
  )
})
