import React, { ComponentProps, useCallback } from 'react'
import classNames from 'classnames'
import { Range as ReactRange } from 'react-range'
import styles from './range.module.scss'

type ReactRangeProps = Partial<
  Omit<
    ComponentProps<typeof ReactRange>,
    'renderTrack' | 'renderMark' | 'renderThumb' | 'values' | 'onChange'
  >
>
type Props = ReactRangeProps & {
  value: number
  onChange(value: number): void
}

export const Range: React.FC<Props> = React.memo((props) => {
  const { value, onChange, ...rest } = props

  const handleChange = useCallback(
    (values: number[]) => {
      if (typeof values[0] === 'number') {
        onChange(values[0])
      }
    },
    [onChange],
  )

  return (
    <div className={styles.wrapper}>
      <ReactRange
        {...rest}
        values={[value]}
        onChange={handleChange}
        renderTrack={({ props, children }) => (
          <div {...props} className={styles.track}>
            {children}
          </div>
        )}
        renderMark={({ index }) => (
          <div className={styles.mark}>
            <div className={styles.mark__number}>{index}</div>
          </div>
        )}
        renderThumb={({ props, isDragged }) => (
          <div
            {...props}
            className={classNames(
              styles.thumb,
              isDragged && styles.thumb_dragged,
            )}
          >
            <div className={styles.thumb__inner} />
          </div>
        )}
      />
    </div>
  )
})

Range.displayName = 'Range'
