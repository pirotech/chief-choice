import React from 'react'
import classNames from 'classnames'
import styles from './button.module.scss'

type Type = 'primary' | 'secondary'

type Props = React.PropsWithChildren &
  React.HTMLAttributes<HTMLButtonElement> & {
    type?: Type
    icon?: React.ReactNode
  }

export const Button: React.FC<Props> = React.memo((props) => {
  const { children, className, type = 'primary', icon, ...rest } = props

  return (
    <button className={classNames(styles.button, styles[`button_${type}`], className)} {...rest}>
      {icon}
      {children}
    </button>
  )
})

Button.displayName = 'Button'
