import { Touch, TouchEventHandler, useCallback, useRef, useState } from "react"

export const useShifted = () => {
  const [shifted, setShifted] = useState(false)
  const startRef = useRef<Touch>()

  const onTouchStart = useCallback<TouchEventHandler>((event) => {
    startRef.current = event.touches[0]
  }, [])

  const onTouchMove = useCallback<TouchEventHandler>((event) => {
    const startClientX = startRef.current?.clientX ?? 0
    setShifted(startClientX > event.touches[0].clientX)
  }, [])

  return { shifted, onTouchStart, onTouchMove }
}
