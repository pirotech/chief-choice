import React from 'react';

export type Styleable = Pick<React.HTMLAttributes<HTMLDivElement>, 'className' | 'style'>;