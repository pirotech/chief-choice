import React, { useCallback, useState } from 'react'
import { PlusCircle } from 'lucide-react'
import { v4 } from 'uuid'
import { Rate, Review } from '../../../../entities/review/model/types'
import { Button } from '../../../../shared/components/button'
import { Modal } from '../../../../shared/components/modal'
import { InputController } from '../../../../shared/components/input'
import { Range } from '../../../../shared/components/range'
import { TextareaController } from '../../../../shared/components/textarea/textarea-controller'
import styles from './add-review.module.scss'
import { useForm } from 'react-hook-form'

type FormType = {
  plate: string
  place: string
  rate: number
  comment: string
}

type Props = {
  onAdd(value: Review): void
}

export const AddReview: React.FC<Props> = React.memo((props) => {
  const { onAdd } = props

  const [modalVisible, setModalVisible] = useState(false)

  const handleModalClose = useCallback(() => setModalVisible(false), [])

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<FormType>()

  const [rate, setRate] = useState(0)

  const onSubmit = useCallback((values: FormType) => {
    console.log(values)
    onAdd({
      id: v4(),
      place: {
        name: values.place,
      },
      plate: {
        name: values.plate,
        rate: Number(rate) as Rate,
        comment: values.comment,
      },
    })
    handleModalClose()
  }, [rate])

  return (
    <>
      <Button
        icon={<PlusCircle />}
        type={'primary'}
        onClick={() => setModalVisible(true)}
      >
        Добавить отзыв
      </Button>
      <Modal visible={modalVisible} title={'Отзыв'} onClose={handleModalClose}>
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
          <InputController
            controllerProps={{
              name: 'plate',
              control,
              rules: { required: true },
            }}
            placeholder={'Блюдо'}
            error={errors.plate?.type === 'required' ? 'Обязательное поле' : ''}
          />
          <InputController
            controllerProps={{
              name: 'place',
              control,
              rules: { required: true },
            }}
            placeholder={'Место'}
            error={errors.place?.type === 'required' ? 'Обязательное поле' : ''}
          />
          <label className={styles.rate}>
            <Range min={0} max={10} step={1} value={rate} onChange={setRate} />
          </label>
          <TextareaController
            controllerProps={{
              name: 'comment',
              control,
              rules: { required: true },
            }}
            placeholder={'Комментарий'}
            error={
              errors.comment?.type === 'required' ? 'Обязательное поле' : ''
            }
          />
          <Button className={styles.saveButton}>Сохранить</Button>
        </form>
      </Modal>
    </>
  )
})

AddReview.displayName = 'AddReview'
