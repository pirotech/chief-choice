import { useCallback, useMemo, useState } from 'react'
import { AddReview } from './features/review/ui/add-review'
import { Review } from './entities/review/model/types'
import { useLocalStorageState } from './shared/hooks/useLocalStorageState'
import { Search } from 'lucide-react'
import styles from './app.module.scss'
import { Input } from './shared/components/input'
import { Card } from './shared/components/card'

function App() {
  const [list, setList] = useLocalStorageState<Review[]>('list', [])
  const [search, setSearch] = useState('')
  const searchedList = useMemo(() => {
    const loweredSearch = search.toLowerCase()
    return (
      list?.filter((item) => {
        const loweredItem = (
          item.place.name +
          item.plate.name +
          item.plate.comment
        ).toLowerCase()
        return loweredItem.includes(loweredSearch)
      }) ?? []
    )
  }, [list, search])

  const handleAdd = useCallback((value: Review) => {
    setList((old) => [...(old ?? []), value])
  }, [])

  const handleRemove = useCallback((id: string) => {
    setList(old => old?.filter(item => item.id !== id))
  }, [])

  return (
    <div className={styles.wrapper}>
      <h1 className={styles.title}>Отзывы на блюда</h1>
      <div className={styles.filters}>
        <Input
          icon={<Search style={{ marginTop: 6 }} />}
          type={'search'}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
      <ul className={styles.list}>
        {searchedList.map((item) => (
          <Card review={item} onRemove={handleRemove} key={item.id} />
        ))}
      </ul>
      <div className={styles.panel}>
        <AddReview onAdd={handleAdd} />
      </div>
    </div>
  )
}

export default App
